import citySkyline from './images/city-skyline.jpg';
import cnTower from './images/cn-tower.jpg';
import newCityHall from './images/new-city-hall.jpg';
import oldCityHall from './images/old-city-hall.jpg';
import rogersCenter from './images/rogers-center.jpg';
import theRom from './images/rom.jpg';
import './App.css';
import React, { useState } from 'react';



function App() {
  const images = [citySkyline, oldCityHall, cnTower, theRom, rogersCenter, newCityHall];
  const [selectedImage, setSelectedImage] = useState(null);
  let highLight = (x) => {
      document.getElementById(x).style.display = "block";
  }
  let lowLight = (x) => {
    if(window.innerWidth > 659) {
      document.getElementById(x).style.display = "none";
    } 
    if(window.innerWidth < 659) {
      document.getElementById(x).style.display = "block";
    }
  }

  return (
    <div className="App">
      <header className="App-header">
      <h1 className="mainHead">
          The City of <strong>Toronto</strong>
        </h1>
        <p className="subHead">Historical Sites & <strong>More</strong></p>
      </header>
      <section className="grid">

        <div className="landmarkContainer" 
          onMouseOver={()=>highLight("hoverInfo0")} onMouseOut={()=>lowLight("hoverInfo0")} 
          onClick={()=>setSelectedImage(images[0])}>

          <div id="hoverInfo0" className="hoverInfo">
            <p className="landmark"><strong>Toronto</strong></p>
            <p className="landmarkSub">The Skyline</p>
          </div>
          <img className="torPic" src={images[0]} alt="Cityscape veiw of downtown Toronto." />
        </div>

        <div className="landmarkContainer" 
          onMouseOver={()=>highLight("hoverInfo1")} onMouseOut={()=>lowLight("hoverInfo1")} 
          onClick={()=>setSelectedImage(images[1])}>
          <div id="hoverInfo1" className="hoverInfo">
            <p className="landmark"><strong>Old City Hall</strong></p>
            <p className="landmarkSub">The Clock Tower</p>
          </div>
          <img className="torPic" src={images[1]} alt="Old City Hall in downtown Toronto." />
        </div>

        <div className="landmarkContainer" 
          onMouseOver={()=>highLight("hoverInfo2")} onMouseOut={()=>lowLight("hoverInfo2")} 
          onClick={()=>setSelectedImage(images[2])}>
          <div id="hoverInfo2" className="hoverInfo">
            <p className="landmark"><strong>The CN Tower</strong></p>
            <p className="landmarkSub">One of the worlds <span className="breakHide"><br /></span>Tallest Towers</p>
          </div>
          <img className="torPic" src={images[2]} alt="CN Tower between buildins in downtown Toronto." />
        </div>

        <div className="landmarkContainer" 
          onMouseOver={()=>highLight("hoverInfo3")} onMouseOut={()=>lowLight("hoverInfo3")} 
          onClick={()=>setSelectedImage(images[3])}>
          <div id="hoverInfo3" className="hoverInfo">
            <p className="landmark"><strong>Royal Ontario Museum</strong></p>
            <p className="landmarkSub">Most visited Museum <span className="breakHide"><br /></span>in Canada</p>
          </div>
          <img className="torPic" src={images[3]} alt="Royal Ontaio Museum Architecture." />
        </div>

        <div className="landmarkContainer" 
          onMouseOver={()=>highLight("hoverInfo4")} onMouseOut={()=>lowLight("hoverInfo4")} 
          onClick={()=>setSelectedImage(images[4])}>
          <div id="hoverInfo4" className="hoverInfo">
            <p className="landmark"><strong>Rogers Center</strong></p>
            <p className="landmarkSub">Toronto's Iconic<span className="breakHide"><br /></span> Skydome</p>
          </div>
          <img className="torPic" src={images[4]} alt="Arial view of baseball game at Rogers Center in Toronto." />
        </div>

        <div className="landmarkContainer" 
          onMouseOver={()=>highLight("hoverInfo5")} onMouseOut={()=>lowLight("hoverInfo5")} 
          onClick={()=>setSelectedImage(images[5])}>
          <div id="hoverInfo5" className="hoverInfo">
            <p className="landmark"><strong>Toronto's City Hall</strong></p>
            <p className="landmarkSub"> A Brutalist Architecture<span className="breakHide"><br /></span> Masterpeice</p>
          </div>
          <img className="torPic" src={images[5]} alt="Upwards view of New Toronto City Hall buildings." />
        </div>
      </section>

      <section className="App-footer">
        <ul className="icon-list">
          <li>
            <a className="socialLink" target="_blank" rel="noopener noreferrer" 
            href="https://twitter.com/cityoftoronto?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">
              <i className="fa-brands fa-twitter"></i>
            </a>
          </li>
          <li>
            <a className="socialLink" target="_blank"  rel="noopener noreferrer" 
            href="https://www.facebook.com/cityofto">
              <i className="fa-brands fa-facebook-f"></i>
            </a>
          </li>
          <li>
            <a className="socialLink" target="_blank" rel="noopener noreferrer" 
            href="https://ca.linkedin.com/company/city-of-toronto">
              <i className="fa-brands fa-linkedin-in"></i>
            </a>
          </li>
          <li>
            <a className="socialLink" target="_blank" rel="noopener noreferrer" 
            href="https://www.instagram.com/cityofto/?hl=en">
              <i className="fa-brands fa-instagram"></i>
            </a>
          </li>
        </ul>
      </section>
      <div id='overlay' style={{visibility: selectedImage ? 'visible': 'hidden'}}>
        <div id='overlayContent'>
          <h1 className="close" onClick={ ()=>setSelectedImage(null) }><i class="fa-solid fa-circle-xmark"></i></h1>
          <img src={selectedImage} alt=""/>
        </div>
      </div>
    </div>
  );
}

export default App;
